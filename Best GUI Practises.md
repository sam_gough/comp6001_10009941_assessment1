# Best GUI Practises for VR #

I have found the following 8 practises for GUI within a virtual environment. 

## 1. Press Start ##

The first GUI principle I have found and probably the most important is to have a start screen that allows the user to acclimatise to the environment and then let them start the game when they are ready.  
It gives them an opportunity to get used to their environment and allows them to mentally adjust to the sensation of the virtual world.  
[Source](http://docs.viromedia.com/docs/design-principles)

---

## 2. People don't like getting sick ##

People don't like feeling sick, its a fact of life. In VR the camera is mapped to the players head. Therefore you shouldn't force the user to move their head involuntarily.  
You should also not change the environment against head movement. If you require the user to look somewhere outside of their immediate vision you should gently guide their focus where you want it and let them move to view the object.  
[Source](http://realityshift.io/blog/ui-ux-design-patterns-in-virtual-reality)

---

## 3. Your in a sphere! ##

When designing the UI we need to remember that unlike tradtional flat screen applications, VR is in a sphere. This means the user can't see what is happening outside of their immedite field of view.  
Therefore it is pointless having UI behind the persons back as they won't know its there.  
[Source](http://realityshift.io/blog/ui-ux-design-patterns-in-virtual-reality)

---

## 4. Narrowing the field ##

You shouldn't put any meaningful control elements on the periphery of peoples vision as our vision isn't sharp enough to make it out properly. Controls should be placed in a 1:1 rectangle area.  
[Source](http://realityshift.io/blog/ui-ux-design-patterns-in-virtual-reality)

---

## 5. Something Doesn't Belong ##

Objects that don't make sense to the current scene shouldn't be included. An example would be that if you opened a door in a building and walked into a room and a shark was swimming around in mid air.  
If you project has nothing to do with floating sharks inside a building then this just serves to confuse the player and makes them disjointed from the intended experience.  
[Source](http://realityshift.io/blog/ui-ux-design-patterns-in-virtual-reality)

---

## 6. Too Close, Too Far? ##

You need to be careful with where you place objects within the scene. If you place objects too close to the users face they will become blurry.  
Conversly if you place the object too far away people find it diffucult to determine the distances between them. Need to be careful to place objects you want the user to interact with in the right place.  
[Source](http://realityshift.io/blog/ui-ux-design-patterns-in-virtual-reality)

---

## 7. Behaviour ##

In a VR environment objects should behave as the user expects it to behave. They should not have counter intuitive actions attached to them.  
For example if you picked up a rock and threw it, it should fall back to the ground as you would expect not start shooting flames out the back and start accelerating.  
[Source](https://madewith.unity.com/stories/secrets-of-a-super-spy-vr-game)

---

## 8. Scale  ##

Scale is also very important to condider when making a project. You can have a space that is too big and the player could get lost within it, or if you make it too small they could suffer from claustrophobia.  
There may be some situations where these are required if it provides a certain atmosphere to your project. But in general you should make sure the scale of the environment is relevant to what is happening.  
There is no point having a 2km squared map if all you have in it is one small box to move around.  
[Source](https://blog.prototypr.io/designing-for-vr-a-beginners-guide-d2fe37902146)
