## Peer Review ##

He has used the aspects of his report such as having things that belong to the application. There are no objects that are unrelated to distract you. 
Because it is a simple application he has not included any sort of movement so there is no risk of motion sickness.  He has also made sure everything is within
the user�s immediate view so they don�t have to search for what to do.

VR is a new experience for me yet Sam�s app was extremely easy to use and come to grips with, even for a novice like me. 

Thanks to the tutorial which loaded on start-up, I was able to quickly acclimatise to the environment.The instructions we�re clear and easy to follow and launched 
you into the next screen once you completed the tutorial which was another great feature.

I did not need to search the environment to find what to do, everything was right in front which made the experience effortless.
A great simple easy to use app. Thanks Sam

Peer Review:
Johnny Kingi / 10005351
